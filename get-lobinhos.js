const urlApi = 'https://lobinhos-api.herokuapp.com/wolves'

const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

fetch(urlApi)
.then (parseRequestToJSON)
.then((responseAsJSON) => {
    let lobinho = document.createElement('div');
    lobinho.className = 'lobinho';

    let img = document.createElement('img');
    img.className = 'lobinho-img'
    img.src = `${responseAsJSON[1].photo}`;

    let name = document.createElement('h4');
    name.innerHTML = `${responseAsJSON[1].name}`

    let age = document.createElement('h5');
    age.innerHTML = `Idade: ${responseAsJSON[1].age} anos`;

    let description = document.createElement('p');
    description.innerHTML = `${responseAsJSON[1].description}`;

    lobinho.appendChild(img);
    lobinho.appendChild(name);
    lobinho.appendChild(age);
    lobinho.appendChild(description);
 
    const allLobinhos = document.querySelector('#all-lobinhos');
    allLobinhos.appendChild(lobinho);

    let lobinho2 = document.createElement('div');
    lobinho2.className = 'lobinho';

    let img2 = document.createElement('img');
    img2.className = 'lobinho-img'
    img2.src = `${responseAsJSON[0].photo}`;

    let name2 = document.createElement('h4');
    name2.innerHTML = `${responseAsJSON[0].name}`

    let age2 = document.createElement('h5');
    age2.innerHTML = `Idade: ${responseAsJSON[0].age} anos`;

    let description2 = document.createElement('p');
    description2.innerHTML = `${responseAsJSON[0].description}`;

    lobinho2.appendChild(img2);
    lobinho2.appendChild(name2);
    lobinho2.appendChild(age2);
    lobinho2.appendChild(description2);

    allLobinhos.appendChild(lobinho2);
});