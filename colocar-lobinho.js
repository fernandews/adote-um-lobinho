const button = document.querySelector('.post');
button.addEventListener('click', (e) => {
    e.preventDefault();
});

const parseRequestToJSON = (response) => {
    response.json();
}

let imgLink = document.querySelector('.img-link');
let inputAge = document.querySelector('.wolf-age');
let inputName = document.querySelector('.wolf-name');
let inputDescription = document.querySelector('.description');

button.addEventListener('click', () => {
    let name = inputName.value;
    let description = inputDescription.value
    let age = inputAge.value
    let photo = imgLink.value;
 
    let header = {
        "Content-Type": "application/json"
    }

    let newwolf = {
        "wolf": {
          "photo": `${photo}`,
          "age": `${age}`,
          "name": `${name}`,
          "description": `${description}`,
        }
    };

    fetch('https://lobinhos-api.herokuapp.com/wolves', {
        method: 'POST',
        headers: header,
        body: JSON.stringify(newwolf)
    });
});